import Vue from 'vue'
import App from './App.vue'
import VueGoodTablePlugin from 'vue-good-table';
import 'vue-good-table/dist/vue-good-table.css'
// import 'bootstrap';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import jQuery from 'jquery';

Vue.use(VueGoodTablePlugin);


Vue.config.productionTip = false
new Vue({
  render: h => h(App),
}).$mount('#app')

